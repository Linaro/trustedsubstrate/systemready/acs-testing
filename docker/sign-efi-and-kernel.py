#!/usr/bin/env python3

import argparse
import guestfs
import logging
import os
import pathlib
import shutil
import subprocess


logger = logging.getLogger()
logger.setLevel(os.getenv("DEBUG") and logging.DEBUG or getattr(logging, os.getenv("LOGLEVEL", "INFO")))
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(ch)


def parse_args():
    parser = argparse.ArgumentParser(description="Sign EFI binaries in an ACS-IR image")
    parser.add_argument("--acs", required=True, help="Path to the ACS-IR wic image")
    parser.add_argument( "--key", required=True, help="Path to *.key file")
    parser.add_argument( "--cert", required=True, help="Path to *.crt file")
    parser.add_argument( "--debug", action="store_true", default=False, help="Display debug messages")
    return parser.parse_args()


def main():
    args = parse_args()

    if args.debug:
        logger.setLevel(level=logging.DEBUG)

    logger.info(f"Signing \"{args.acs}\" with \"{args.key}\" and \"{args.cert}\"")

    guest = guestfs.GuestFS(python_return_dict=True)
    guest.add_drive_opts(args.acs, format="raw")

    logger.debug(f"Starting VM with \"{args.acs}\" mounted")
    guest.launch()

    logger.debug("Mounting \"/dev/sda1\"")
    guest.mount("/dev/sda1", "/")

    # Sign "/Image" first
    logger.info("Signing \"Image\"")
    logger.debug("Extracting Image from \"/\"")
    guest.copy_out("/Image", "./")
    subprocess.run(f"sbsign --key {args.key} --cert {args.cert} Image --output Image".split())
    logger.debug("Bringing Image back in")
    guest.copy_in("./Image", "/")

    # Sign
    logger.info("Signing efi binaries")
    logger.debug("Extracting files from \"/EFI/BOOT\"")
    guest.copy_out("/EFI/BOOT", "./")

    for efi in pathlib.Path("./BOOT").rglob("*.efi"):
        logger.info(f"Signing \"{efi}\"")
        subprocess.run(f"sbsign --key {args.key} --cert {args.cert} {efi} --output {efi}".split())

    logger.debug("Copying signed files back in \"/EFI/BOOT\"")
    guest.copy_in("./BOOT", "/EFI/")

    logger.debug("Shutting VM down")
    guest.close()


if __name__ == "__main__":
    main()
