Latest ACS Run:
        <a href="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/badge?passrate" />
        </a>

<!--
  Gitlab allows certain HTML tags to be added as Markdown.
  The full list of allowed tags is here: https://github.com/gjtorikian/html-pipeline/blob/a363d620eba0076479faad86f0cf85a56b2b3c0a/lib/html/pipeline/sanitization_filter.rb
  Also note that there cannot be spaces between tags, otherwise HTML parser doesn't work
-->

<table>
  <thead>
    <th></th>
    <th>qemu <sup>TRS</sup></th>
    <th>rockpi4b <sup>TRS</sup></th>
    <th>stm32mp157c-dk2 <sup>TRS</sup></th>
    <th>synquacer <sup>TRS</sup></th>
    <th>kv260 starter kit <sup>TRS</sup></th>
  <thead>
  <tbody>
    <!-- acs suite -->
    <tr>
      <th>acs</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/tests">
          <img height="20" width="70" src="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/badge?environment=qemu&suite=acs&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        N/A
      </td>
      <!-- stm32mp157c-dk2 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/tests">
          <img height="20" width="70" src="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/badge?environment=synquacer&suite=acs&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-kria-starter -->
      <td>
        <a href="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/tests">
          <img height="20" width="70" src="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/badge?environment=kv260&suite=acs&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of build suite -->
  </tbody>
</table>


Legend: 


* <img src="https://qa-reports.linaro.org/trustedsubstrate/acs-testing/build/latest-finished/badge?environment=donotexist&suite=donotexist&passrate&title&hide_zeros=1" /> : No results yet, tests are in progress

# ACS Testing of Trusted Substrate

This repository contains a weekly pipeline that runs ACS tests on Trusted Substrate firmwares.
For now, we'll be running only on Qemu.

## Signing ACS Image (manually on demand)

The only requirement for signing the ACS image is Docker, then one can run:

```
./scripts/sign-acs-image.sh
```

This script will download the unsigned ACS + signing key and certificate, spin a docker container,
install libguestfs and sign all `EFI` binaries + `Image` from the ACS image. The final signed image
will be saved in `images/` directory and should be pushed to Gitlab for future usages.

For now, signing will be done manually, and we should be checking for updates on the official Github repository.

This is probably **not** the best approach, but should be good enough for enabling other boards on ACS.

## Weekly jobs

There is a scheduled pipeline here in this repository which download latest Qemu firmware from https://gitlab.com/Linaro/blueprints/ci
and spin a LAVA test job using a signed ACS image. The job normally takes several hours to complete
and the final resulting logs should be saved in https://qa-reports.linaro.org/trustedsubstrate/acs-testing/.
