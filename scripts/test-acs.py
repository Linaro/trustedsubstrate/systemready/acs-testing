#!/usr/bin/env python3

from datetime import date
from jinja2 import Environment, FileSystemLoader

import logging
import os
import re
import requests
import sys
import subprocess as sp


logger = logging.getLogger()
logger.setLevel(os.getenv("DEBUG") and logging.DEBUG or getattr(logging, os.getenv("LOGLEVEL", "INFO")))
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter("[%(levelname)s] %(message)s"))
logger.addHandler(ch)


SQUAD_GROUP = "trustedsubstrate"
SQUAD_PROJECT = "acs-testing"
SQUAD_BUILD = str(date.today())
FIRMWARE_URL = {
    "qemu": {
        "flash.bin-qemu.gz": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/flash.bin-qemu.gz?job=build-meta-ts-qemuarm64-secureboot",
    },
    "kv260": {
        "ImageA.bin.gz": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageA.bin.gz?job=build-meta-ts-zynqmp-kria-starter",
        "ImageB.bin.gz": "https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageB.bin.gz?job=build-meta-ts-zynqmp-kria-starter",
    },
    "synquacer": None,
}


def retrieve_real_firmware_url(index_url):
    """
        `FIRMWARE_URL` is a Gitlab's generic URL pointing to the latest successful pipeline.
        We should ping that URL and Gitlab will return a 302 with the actual URL with job id,
        which is the one working in LAVA
    """
    final_urls = {}
    for firmware_name, url in index_url.items():
        logger.info(f"Retrieving URL for {firmware_name}")
        response = requests.get(url, allow_redirects=False)
        if response.status_code != 302:
            logger.error(response.text)
            return None
        final_urls[firmware_name] = response.headers["Location"]
    return final_urls


def run_cmd(cmd, print_stdout=True):
    """
        Run the specified `cmd` and waits for its completion.
        Returns `proc` with extra attributes:
        - `ok` True if the the command returned 0, False otherwise
        - `out` decoded command stdout
        - `err` decoded command stderr
    """
    logger.info(f"Running {cmd}")
    if type(cmd) == str:
        cmd = cmd.split()

    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, bufsize=1, universal_newlines=True)

    stdout = []
    if print_stdout:
        for line in proc.stdout:
            logger.info(line.replace("\n", ""))
            stdout.append(line)

    _, stderr = proc.communicate()
    proc.ok = proc.returncode == 0
    proc.out = "".join(stdout)
    proc.err = stderr
    return proc

    
def generate_lava_job_definition(context, device):
    try:
        templates_dir = ['./templates/']
        env = Environment(loader=FileSystemLoader(templates_dir))
        return env.get_template(f"{device}.yaml.jinja2").render(context)
    except Exception as e:
        logger.warning(f"Could not generate LAVA job definition for {device}: {e}")
        raise e


def send_testjob_request_to_squad(job_definition, environment):
    definition_filename = f"/tmp/{environment}-definition.yml"
    with open(definition_filename, "w") as fp:
        fp.write(job_definition)

    cmd = [
        "squad-client",
        "submit-job",
        "--group", SQUAD_GROUP,
        "--project", SQUAD_PROJECT,
        "--build", SQUAD_BUILD,
        "--backend", "ledge.validation.linaro.org",
        "--environment", environment,
        "--definition", definition_filename,
    ]

    proc = run_cmd(cmd)
    if not proc.ok:
        logger.warning(f"Could not submit job: {proc.out}, {proc.err}")
        return False, None

    logger.info(f"See test job progress at {os.getenv('SQUAD_HOST')}/{SQUAD_GROUP}/{SQUAD_PROJECT}/build/{SQUAD_BUILD}/testjobs/")

    matches = re.findall(r"SQUAD job id: (\d+)", proc.out + proc.err)
    if len(matches) != 1:
        logger.warning(f"Could not obtain SQUAD job id: match returned {matches}")
        return False, None

    job_id = matches[0]
    return True, job_id


def main():
    devices = os.getenv("DEVICE", "").split(',')
    if devices[0] == "":
        logger.error("Missing `DEVICE` environment variable")
        return False

    results = []
    for device in devices:
        context = {}

        if FIRMWARE_URL[device]:
            firmware_url = retrieve_real_firmware_url(FIRMWARE_URL[device])
            if firmware_url is None:
                logger.error(f"Could not determine firmware URL for {device}")
                return False

        context = {
            "ci_job_id": os.getenv("CI_JOB_ID"),
            "firmware_url": firmware_url,
            "squad_host": os.getenv("SQUAD_HOST"),
            "squad_group": SQUAD_GROUP,
            "squad_project": SQUAD_PROJECT,
            "squad_build": SQUAD_BUILD,
            "squad_device": device,
            "squad_token": os.getenv("SQUAD_TOKEN"),
        }

        definition = generate_lava_job_definition(context, device)
        print(definition)

        ok, _ = send_testjob_request_to_squad(definition, device)
        results.append(ok)

    return all(results)


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
