#!/bin/bash

set -xe

IMAGES_DIR="images"
TMP_DIR="tmp"
ACS="ir-acs-live-image-generic-arm64.wic"
CERT="db.crt"
KEY="db.key"
TS_CERTS_URL="https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/raw/master/meta-trustedsubstrate/uefi-certificates/uefi_certs.tgz"

mkdir -p ${IMAGES_DIR} ${TMP_DIR}

if [ ! -f ${IMAGES_DIR}/${ACS}.xz ]
then
        wget https://github.com/ARM-software/arm-systemready/raw/main/IR/prebuilt_images/v23.09_2.1.0/${ACS}.xz -O - | xz -d > ${IMAGES_DIR}/${ACS}
fi

if [ ! -f ${TMP_DIR}/${CERT} ]
then
        wget ${TS_CERTS_URL}  -O - | tar zxvf - ./${CERT}
        mv ${CERT} ${TMP_DIR}
fi

if [ ! -f ${TMP_DIR}/${KEY} ]
then
        wget ${TS_CERTS_URL} -O - | tar zxvf - ./${KEY}
        mv ${KEY} ${TMP_DIR}
fi

docker \
        run \
        -it \
        --rm \
        --name=ts-signer-container \
        --mount=type=bind,source=${PWD}/,destination=/tmp/acs \
        debian \
                bash -c " \
			apt-get update -q && \
			apt-get install -qy sbsigntool python3-guestfs && \
			/tmp/acs/docker/sign-efi-and-kernel.py --debug \
				--acs  /tmp/acs/${IMAGES_DIR}/${ACS} \
				--key  /tmp/acs/${TMP_DIR}/${KEY} \
				--cert /tmp/acs/${TMP_DIR}/${CERT}"

xz ${IMAGES_DIR}/${ACS}
